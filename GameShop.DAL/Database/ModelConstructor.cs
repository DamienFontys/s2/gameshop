using System;
using System.Linq;
using System.Reflection;

namespace GameShop.DAL.Database

{
    public static class ModelConstructor<T>
    {
        public static T CreateInstance(QueryResult queryResult)
        {
            var model = (T) Activator.CreateInstance(typeof(T));

            var properties = (from t in typeof(T).GetProperties()
                where t.GetCustomAttributes<PropertyAttribute>().Any()
                select t).ToList();

            foreach (var (key, value) in queryResult.Properties)
            {
                var found = properties.Find(prop => prop.Name.ToLower() == key);
                if (found != null)
                {
                    found.SetValue(model, value);
                }
            }

            return model;
        }
    }
}