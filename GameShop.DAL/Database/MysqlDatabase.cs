using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace GameShop.DAL.Database
{
    public sealed class MysqlDatabase
    {
        private static readonly Lazy<MysqlDatabase> DatabaseInstance =
            new Lazy<MysqlDatabase>(() => new MysqlDatabase());

        public static MysqlDatabase Instance => DatabaseInstance.Value;

        private MySqlConnection _databaseConnection;

        public void SetConnection(string host, string username, string password, string database)
        {
            _databaseConnection = new MySqlConnection("server=" + host + ";user id=" + username + ";password=" +
                                                      password + ";database=" + database);
        }

        private void OpenConnection()
        {
            try
            {
                _databaseConnection.Open();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw new Exception("Could not connect to database");
            }
        }

        private void CloseConnection()
        {
            try
            {
                _databaseConnection.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw new Exception("Could not disconnect from database");
            }
        }

        public List<QueryResult> Query(string query, List<object> parameters)
        {
            var results = new List<QueryResult>();
            var databaseQuery = new MySqlCommand(query, _databaseConnection);

            if (parameters != null)
            {
                var param = 0;
                foreach (var parameter in parameters)
                {
                    databaseQuery.Parameters.AddWithValue($"param{param}", parameter);
                    param++;
                }
            }

            OpenConnection();
            try
            {
                var dataReader = databaseQuery.ExecuteReader();

                while (dataReader.Read())
                {
                    var result = new QueryResult(dataReader);
                    results.Add(result);
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                CloseConnection();
            }

            return results;
        }

        public List<QueryResult> Query(string query, params object[] parameters)
        {
            var results = new List<QueryResult>();
            var databaseQuery = new MySqlCommand(query, _databaseConnection);

            if (parameters != null)
            {
                for (var i = 1; i < parameters.Length + 1; i++)
                {
                    databaseQuery.Parameters.AddWithValue("param" + i, parameters[i - 1]);
                }
            }

            OpenConnection();
            try
            {
                var dataReader = databaseQuery.ExecuteReader();

                while (dataReader.Read())
                {
                    var result = new QueryResult(dataReader);
                    results.Add(result);
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                CloseConnection();
            }

            return results;
        }

        public long Scalar(string query, List<string> parameters)
        {
            var databaseQuery = new MySqlCommand(query, _databaseConnection);

            if (parameters != null)
            {
                var param = 0;
                foreach (var parameter in parameters)
                {
                    databaseQuery.Parameters.AddWithValue($"param{param}", parameter);
                    param++;
                }
            }

            OpenConnection();
            try
            {
                databaseQuery.ExecuteScalar();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                CloseConnection();
            }

            return databaseQuery.LastInsertedId;
        }

        public List<QueryResult> Procedure(string procedure, params object[] parameters)
        {
            var results = new List<QueryResult>();
            var databaseQuery = new MySqlCommand(procedure, _databaseConnection);
            databaseQuery.CommandType = CommandType.StoredProcedure;

            foreach (var parameter in parameters)
            {
                databaseQuery.Parameters.AddWithValue($"param{(Array.IndexOf(parameters, parameter) + 1).ToString()}",
                    parameter);
            }


            OpenConnection();

            try
            {
                var dataReader = databaseQuery.ExecuteReader();
                while (dataReader.Read())
                {
                    var result = new QueryResult(dataReader);
                    results.Add(result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                CloseConnection();
            }

            return results;
        }
    }
}