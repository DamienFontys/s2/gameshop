using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.DAL
{
    public interface IContext<T> where T : Model
    {
        T GetById(int id);

        IEnumerable<T> GetAll();

        int Add(T model);
        void Remove(T model);
        void Update(T model);
    }
}