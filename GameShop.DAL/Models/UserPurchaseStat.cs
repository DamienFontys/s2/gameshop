using System;
using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class UserPurchaseStat: Model
    {
        [Property]
        public string Email { get; set; }
        [Property]
        public long Purchases { get; set; }
    }
}