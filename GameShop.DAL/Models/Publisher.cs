using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Publisher : Model
    {
        [Property] public string Name { get; set; }
    }
}