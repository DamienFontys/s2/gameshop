using System.Diagnostics.CodeAnalysis;
using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Review : Model
    {
        [Property] public string Content { get; set; }

        [Property][ExcludeFromCodeCoverage] public int Likes { get; set; }

        [Property][ExcludeFromCodeCoverage] public int Dislikes { get; set; }

        [Property] public uint Game_Id { get; set; }

        [Property] public uint Author_Id { get; set; }

        public User Author { get; set; }
    }
}