using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class GamePurchaseStat
    {
        [Property]
        public string Game { get; set; }

        [Property]
        public long Purchases { get; set; }
    }
}