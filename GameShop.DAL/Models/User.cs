using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class User : Model
    {
        [Property] public string Email { get; set; }
        [Property] public string Password { get; set; }

        [Property] public string Firstname { get; set; }
        [Property] public string Lastname { get; set; }

        [Property] public int GBucks { get; set; }
        
        [Property] public string Lightningid { get; set; }

        public IEnumerable<Review> Reviews { get; set; }

        public string GetGravatarHash()
        {
            var hash = new StringBuilder();
            var md5Provider = new MD5CryptoServiceProvider();
            var bytes = md5Provider.ComputeHash(new UTF8Encoding().GetBytes(Email));

            foreach (var t in bytes)
            {
                hash.Append(t.ToString("x2"));
            }

            return hash.ToString();
        }
    }
}