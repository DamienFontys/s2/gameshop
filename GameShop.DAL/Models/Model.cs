using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Model
    {
        [Property] public uint Id { get; set; }
    }
}