using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Purchase : Model
    {
        [Property] public uint User_Id { get; set; }

        [Property] public uint game_id { get; set; }

        [Property] public string gamekey { get; set; }

        public Game Game { get; set; }

        public User User { get; set; }
    }
}