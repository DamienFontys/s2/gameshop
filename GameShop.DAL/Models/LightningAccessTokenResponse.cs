namespace GameShop.DAL.Models
{
    public class LightningAccessTokenResponse
    {
        public string access_token { get; set; }
    }
}