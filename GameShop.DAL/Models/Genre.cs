using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Genre : Model
    {
        [Property] public string Title { get; set; }
    }
}