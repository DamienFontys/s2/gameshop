﻿using System.Collections.Generic;
using GameShop.DAL.Database;

namespace GameShop.DAL.Models
{
    public class Game : Model
    {
        [Property] public string Title { get; set; }
        [Property] public string Description { get; set; }
        [Property] public double Price { get; set; }
        [Property] public int CoinPrice { get; set; }

        [Property] public uint Genre_Id { get; set; }

        [Property] public uint Publisher_Id { get; set; }

        public List<Review> Reviews { get; set; } = new List<Review>();
    }
}