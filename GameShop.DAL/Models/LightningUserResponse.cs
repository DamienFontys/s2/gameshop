namespace GameShop.DAL.Models
{
    public class LightningUserResponse
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
    }
}