using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GameShop.DAL.Database;
using GameShop.DAL.Models;

namespace GameShop.DAL
{
    public class MysqlContext<T> : IContext<T> where T : Model
    {
        public T GetById(int id)
        {
            var result = MysqlDatabase.Instance.Query($"SELECT * FROM {typeof(T).Name.ToLower()} WHERE id = ?", id);
            return !result.Any() ? null : ModelConstructor<T>.CreateInstance(result.First());
        }

        public IEnumerable<T> GetAll()
        {
            var results = MysqlDatabase.Instance.Query($"SELECT * FROM {typeof(T).Name.ToLower()}");

            foreach (var result in results)
            {
                yield return ModelConstructor<T>.CreateInstance(result);
            }
        }

        public int Add(T model)
        {
            var properties = (from t in typeof(T).GetProperties()
                where t.GetCustomAttributes<PropertyAttribute>().Any()
                select t).ToList();


            var columns = new List<string>();
            var values = new List<string>();
            var placeholders = new List<string>();

            foreach (var property in properties)
            {
                if (property.Name == "Id") continue;
                columns.Add(property.Name.ToLower());
                values.Add(property.GetValue(model).ToString());
                placeholders.Add("?");
            }

            var id = MysqlDatabase.Instance.Scalar(
                $"INSERT INTO {typeof(T).Name.ToLower()} ({string.Join(", ", columns)}) VALUES ({string.Join(", ", placeholders)})",
                values);
            return int.Parse(id.ToString());
        }

        public void Remove(T model)
        {
            MysqlDatabase.Instance.Query($"DELETE FROM {typeof(T).Name.ToLower()} WHERE id = ?", model.Id);
        }

        public void Update(T model)
        {
            var properties = (from t in typeof(T).GetProperties()
                where t.GetCustomAttributes<PropertyAttribute>().Any()
                select t).ToList();

            var updates = new List<string>();
            var values = new List<object>();
            var placeholders = new List<string>();

            foreach (var property in properties)
            {
                if (property.Name == "Id") continue;
                updates.Add($"{property.Name.ToLower()} = ?");
                values.Add(property.GetValue(model));
                placeholders.Add("?");
            }

            values.Add(model.Id);

            MysqlDatabase.Instance.Query($"UPDATE user SET {string.Join(", ", updates)} WHERE id = ?", values);
        }
    }
}