using System.Collections.Generic;
using System.Linq;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Repositories
{
    public class GameRepository : Repository<Game>, IGameRepository
    {
        private readonly IGameContext _gameContext;

        public GameRepository(IGameContext gameContext) : base(gameContext)
        {
            _gameContext = gameContext;
        }

        public IEnumerable<Game> SearchForGame(int genreId, string keyword, int publisherId)
        {
            var games = _gameContext.GetAll();

            if (genreId != 0 && publisherId == 0)
            {
                games = games.Where(game => game.Genre_Id == genreId);
            }

            if (publisherId != 0 && genreId == 0)
            {
                games = games.Where(game => game.Publisher_Id == publisherId);
            }

            if (publisherId != 0 && genreId != 0)
            {
                games = games.Where(game => game.Publisher_Id == publisherId && game.Genre_Id == genreId);
            }

            if (keyword != null)
            {
                games = games.Where(game => game.Title.ToLower().Contains(keyword.ToLower()));
            }

            return games;
        }

        public Game GetGameByIdWithReviews(int id)
        {
            return _gameContext.GetGameByIdWithReviews(id);
        }
    }
}