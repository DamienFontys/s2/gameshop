using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IUserContext _userContext;

        public UserRepository(IUserContext userContext) : base(userContext)
        {
            _userContext = userContext;
        }

        public User GetUserByEmail(string email)
        {
            return _userContext.GetUserByEmail(email);
        }

        public User GetUserByLightningId(string lightningId)
        {
            return _userContext.GetUserByLightningId(lightningId);
        }
        
        public void RegisterUser(User user)
        {
            _userContext.RegisterUser(user);
        }
    }
}