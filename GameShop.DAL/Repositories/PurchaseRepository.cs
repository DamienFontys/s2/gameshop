using System.Collections.Generic;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Repositories
{
    public class PurchaseRepository : Repository<Purchase>, IPurchaseRepository
    {
        private readonly IPurchaseContext _purchaseContext;

        public PurchaseRepository(IPurchaseContext purchaseContext) : base(purchaseContext)
        {
            _purchaseContext = purchaseContext;
        }

        public IEnumerable<Purchase> GetPurchasesForUser(User user)
        {
            return _purchaseContext.GetPurchasesForUser(user);
        }

        public IEnumerable<UserPurchaseStat> GetUserPurchaseStats()
        {
            return _purchaseContext.GetUserPurchaseStats();
        }

        public IEnumerable<GamePurchaseStat> GetGamePurchaseStats()
        {
            return _purchaseContext.GetGamePurchaseStats();
        }
    }
}