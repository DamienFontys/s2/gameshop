using System.Linq;
using GameShop.DAL.Database;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Contexts
{
    public class MysqlUserContext : MysqlContext<User>, IUserContext
    {
        public User GetUserByEmail(string email)
        {
            var result = MysqlDatabase.Instance.Query("SELECT * FROM user WHERE email = ?", email).FirstOrDefault();

            if(result == null){
                return null;
            }

            return ModelConstructor<User>.CreateInstance(result);
        }

        public User GetUserByLightningId(string lightningId)
        {
            var result = MysqlDatabase.Instance.Query("SELECT * FROM user where lightningid = ?", lightningId).FirstOrDefault();

            if (result == null)
            {
                return null;
            }

            return ModelConstructor<User>.CreateInstance(result);
        }

        public void RegisterUser(User user)
        {
            MysqlDatabase.Instance.Procedure("RegisterUser", user.Email, user.Password, user.Firstname, user.Lastname, user.Lightningid);
        }
    }
}