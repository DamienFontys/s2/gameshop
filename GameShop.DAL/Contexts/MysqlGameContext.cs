using System.Linq;
using GameShop.DAL.Database;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Contexts
{
    public class MysqlGameContext : MysqlContext<Game>, IGameContext
    {
        public Game GetGameByIdWithReviews(int id)
        {
            var game = new Game();
            var results = MysqlDatabase.Instance.Procedure("GetGameByIdWithReviewsWithAuthor", id);

            foreach (var result in results)
            {
                if (results.First() == result)
                {
                    game = ModelConstructor<Game>.CreateInstance(result);
                }

                if (result["review_id"] == null) continue;

                var reviewContent = (string) result["review"];

                var authorEmail = (string) result["author_email"];
                var authorFirstName = (string) result["author_firstname"];
                var authorLastName = (string) result["author_lastname"];

                var author = new User
                    {Email = authorEmail, Firstname = authorFirstName, Lastname = authorLastName};

                var review = new Review {Content = reviewContent, Author = author};

                game.Reviews.Add(review);
            }

            return game;
        }
    }
}