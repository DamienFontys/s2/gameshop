using System.Collections.Generic;
using GameShop.DAL.Database;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.DAL.Contexts
{
    public class MysqlPurchaseContext : MysqlContext<Purchase>, IPurchaseContext
    {
        public IEnumerable<Purchase> GetPurchasesForUser(User user)
        {
            var results = MysqlDatabase.Instance.Query(
                "SELECT purchase.*, game.title as title, game.id as game FROM purchase INNER JOIN game on purchase.game_id = game.id WHERE user_id = ?;",
                user.Id);

            foreach (var result in results)
            {
                var purchase = ModelConstructor<Purchase>.CreateInstance(result);

                purchase.Game = new Game {Id = (uint) result["game"], Title = (string) result["title"]};

                yield return purchase;
            }
        }

        public IEnumerable<UserPurchaseStat> GetUserPurchaseStats()
        {
            var results = MysqlDatabase.Instance.Query(
                "SELECT user.email as email, COUNT(user_id) as purchases FROM purchase INNER JOIN user ON purchase.user_id = user.id GROUP BY user.email;");

            foreach (var result in results)
            {
                var purchaseStat = ModelConstructor<UserPurchaseStat>.CreateInstance(result);
                yield return purchaseStat;
            }
        }

        public IEnumerable<GamePurchaseStat> GetGamePurchaseStats()
        {
            var results = MysqlDatabase.Instance.Query(
                "SELECT game.title as game, COUNT(game_id) as purchases FROM purchase INNER JOIN game ON purchase.game_id = game.id GROUP BY game.title;");

            foreach (var result in results)
            {
                var purchaseStat = ModelConstructor<GamePurchaseStat>.CreateInstance(result);
                yield return purchaseStat;
            }
        }
    }
}