using System.Collections.Generic;

namespace GameShop.DAL
{
    public interface IRepository<T>
    {
        T GetById(int id);

        IEnumerable<T> GetAll();

        int Add(T model);

        void Remove(T model);

        void Update(T model);
    }
}