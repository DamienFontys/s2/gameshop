using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.DAL
{
    public class Repository<T> : IRepository<T> where T : Model
    {
        private readonly IContext<T> _context;

        public Repository(IContext<T> context)
        {
            _context = context;
        }

        public T GetById(int id)
        {
            return _context.GetById(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.GetAll();
        }

        public int Add(T model)
        {
            return _context.Add(model);
        }

        public void Remove(T model)
        {
            _context.Remove(model);
        }

        public void Update(T model)
        {
            _context.Update(model);
        }
    }
}