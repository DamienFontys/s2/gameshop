using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IGameRepository: IRepository<Game>
    {
        IEnumerable<Game> SearchForGame(int genreId, string keyword, int publisherId);
        Game GetGameByIdWithReviews(int id);
    }
}