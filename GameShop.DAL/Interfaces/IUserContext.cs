using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IUserContext : IContext<User>
    {
        User GetUserByEmail(string email);

        User GetUserByLightningId(string lightningId);
        
        void RegisterUser(User user);
    }
}