using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IPurchaseRepository: IRepository<Purchase>
    {
        IEnumerable<Purchase> GetPurchasesForUser(User user);

        IEnumerable<UserPurchaseStat> GetUserPurchaseStats();
        IEnumerable<GamePurchaseStat> GetGamePurchaseStats();
    }
}