using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IGameContext : IContext<Game>
    {
        Game GetGameByIdWithReviews(int id);
    }
}