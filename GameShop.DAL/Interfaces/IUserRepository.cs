using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IUserRepository: IRepository<User>
    {
        User GetUserByEmail(string email);
        User GetUserByLightningId(string lightningId);
        void RegisterUser(User user);
    }
}