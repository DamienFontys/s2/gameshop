using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.DAL.Interfaces
{
    public interface IPurchaseContext : IContext<Purchase>
    {
        IEnumerable<Purchase> GetPurchasesForUser(User user);

        IEnumerable<UserPurchaseStat> GetUserPurchaseStats();
        IEnumerable<GamePurchaseStat> GetGamePurchaseStats();
    }
}