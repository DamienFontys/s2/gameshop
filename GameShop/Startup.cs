﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using GameShop.BAL;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL;
using GameShop.DAL.Contexts;
using GameShop.DAL.Database;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
namespace GameShop
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options => { options.LoginPath = "/login"; });

            //Game
            services.AddTransient<IGameContext, MysqlGameContext>();
            services.AddTransient<IGameRepository, GameRepository>();
            services.AddTransient<IGameLogic, GameLogic>();


            //User
            services.AddTransient<IUserContext, MysqlUserContext>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserLogic, UserLogic>();

            //Purchase
            services.AddTransient<IPurchaseContext, MysqlPurchaseContext>();
            services.AddTransient<IPurchaseRepository, PurchaseRepository>();
            services.AddTransient<IPurchaseLogic, PurchaseLogic>();

            //Publisher
            services.AddTransient<IContext<Publisher>, MysqlContext<Publisher>>();
            services.AddTransient<IRepository<Publisher>, Repository<Publisher>>();
            services.AddTransient<ILogic<Publisher>, Logic<Publisher>>();

            //Genre
            services.AddTransient<IContext<Genre>, MysqlContext<Genre>>();
            services.AddTransient<IRepository<Genre>, Repository<Genre>>();
            services.AddTransient<ILogic<Genre>, Logic<Genre>>();

            //Review
            services.AddTransient<IContext<Review>, MysqlContext<Review>>();
            services.AddTransient<IRepository<Review>, Repository<Review>>();
            services.AddTransient<ILogic<Review>, Logic<Review>>();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseSession();
            
            app.Use((context, next) =>
            {
                var query = context.Request.Query;

                foreach (var (key, value) in query)
                {
                    context.Session.Set(key, value);
                }
                return next();
            });
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "Index",
                    "/",
                    new {controller = "Home", action = "Index"});

                routes.MapRoute(
                    "Stats",
                    "/stats",
                    new {controller = "Home", action = "Stats"});
               
                routes.MapRoute(
                    "Login",
                    "/login",
                    new {controller = "Auth", action = "Login"});
               
                routes.MapRoute(
                    "Register",
                    "/register",
                    new {controller = "Auth", action = "Register"});

                routes.MapRoute(
                    "Logout",
                    "/logout",
                    new {controller = "Auth", action = "Logout"});

                routes.MapRoute(
                    "Callback",
                    "/callback",
                    new {controller = "Auth", action="Callback"});
                
                routes.MapRoute(
                    "AllGames",
                    "/games",
                    new {controller = "Games", action = "All"});

                routes.MapRoute(
                    "GetGame",
                    "/games/{id}",
                    new {controller = "Games", action = "Get"});

                routes.MapRoute(
                    "PurchaseGameWithMoney",
                    "/games/{id}/purchaseWithMoney",
                    new {controller = "Games", action = "BuyWithMoney"});

                routes.MapRoute(
                    "PurchaseGameWithBucks",
                    "/games/{id}/purchaseWithBucks",
                    new {controller = "Games", action = "BuyWithBucks"});

                routes.MapRoute(
                    "Review",
                    "/games/{id}/review",
                    new {controller = "Games", action = "Review"});

                routes.MapRoute(
                    "Account",
                    "/account",
                    new {controller = "Account", action = "Account"}
                );

                routes.MapRoute(
                    "Purchases",
                    "/purchases",
                    new {controller = "Account", action = "Purchases"});

                //Not Found
                routes.MapRoute(
                    "Error",
                    "{*.}",
                    new {controller = "Home", action = "Error404"}
                );
            });
            
            //TODO fix environment files being hardcoded
            MysqlDatabase.Instance.SetConnection("ftp.dawreck.nl",
                "u1005p1492_website", "website1122",
                "u1005p1492_gameshop");
        }
    }
    
    [ExcludeFromCodeCoverage]
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : 
                JsonConvert.DeserializeObject<T>(value);
        }
    }
}