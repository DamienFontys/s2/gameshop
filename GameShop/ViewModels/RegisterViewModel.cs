using System.ComponentModel.DataAnnotations;

namespace GameShop.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Please enter your Email")]
        [EmailAddress(ErrorMessage = "Please fill in a valid email")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Please enter a Password")]
        [MinLength(5, ErrorMessage = "Password has to be atleast 5 characters")]
        [MaxLength(50, ErrorMessage = "Password can only be 50 characters")]
        public string Password { get; set; }
        
        [Required(ErrorMessage = "Please enter your First name")]
        [MinLength(2, ErrorMessage = "Firstname has to be atleast 2 characters")]
        [MaxLength(30, ErrorMessage = "Firstname can only be 30 characters")]
        public string Firstname { get; set; }
        
        [Required(ErrorMessage = "Please enter your Last name")]
        [MinLength(2, ErrorMessage = "Lastname has to be atleast 2 characters")]
        [MaxLength(30, ErrorMessage = "Lastname can only be 30 characters")]
        public string Lastname { get; set; }
    }
}