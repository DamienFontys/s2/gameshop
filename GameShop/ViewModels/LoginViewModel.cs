using System.ComponentModel.DataAnnotations;

namespace GameShop.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please fill in an email and password")]
        public string Email{ get; set; }
        [Required(ErrorMessage = "Please fill in a username and password")]
        public string Password{ get; set; }
    }
}