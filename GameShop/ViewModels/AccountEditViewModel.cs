using System.ComponentModel.DataAnnotations;

namespace GameShop.ViewModels
{
    public class AccountEditViewModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string RepeatPassword { get; set; }
    }
}