using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.ViewModels
{
    public class GamesViewModel
    {
        public string Keyword { get; set; }
        public int GenreId { get; set; }
        public int PublisherId { get; set; }
        
        public IEnumerable<Game> Games { get; set; }
        public IEnumerable<Publisher> Publishers { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
    }
}