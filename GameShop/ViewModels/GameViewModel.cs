using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.ViewModels
{
    public class GameViewModel
    {
        public Game Game { get; set; }
    }
}