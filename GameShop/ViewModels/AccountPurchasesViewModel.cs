using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.ViewModels
{
    public class AccountPurchasesViewModel
    {
        public IEnumerable<Purchase> Purchases { get; set; }
    }
}