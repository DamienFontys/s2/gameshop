using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.ViewModels
{
    public class StatsViewModel
    {
        public List<UserPurchaseStat> UserPurchaseStats { get; set; } = new List<UserPurchaseStat>();
        public List<GamePurchaseStat> GamePurchaseStats { get; set; } = new List<GamePurchaseStat>();
    }


    
}