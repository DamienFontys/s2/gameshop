using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace GameShop.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserLogic _userLogic;

        public AuthController(IUserLogic userLogic)
        {
            _userLogic = userLogic;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var dbUser = _userLogic.GetUserByEmail(model.Email);

            if (dbUser == null)
            {
                return RedirectToAction("Login", "Auth", new {error = "Wrong credentials"});
            }
            
            var email = model.Email;
            var password = model.Password;

            if (!await _userLogic.ValidateCredentials(email, password, out var user))
            {
                return RedirectToAction("Login", "Auth", new {error = "Wrong credentials"});
            }

            await _userLogic.LoginUser(HttpContext, user.Email, user.Firstname, user.Lastname);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var dbUser = _userLogic.GetUserByEmail(model.Email);

            if (dbUser != null)
            {
                return RedirectToAction("Index", "Home");
            }
            
            var email = model.Email;
            var password = model.Password;
            var firstname = model.Firstname;
            var lastname = model.Lastname;

            await _userLogic.RegisterUser(email, password, firstname, lastname, null);
            await _userLogic.LoginUser(HttpContext, email, firstname, lastname);
            
            return RedirectToAction("Index", "Home");
        }
        
        [HttpGet]
        public async Task<IActionResult> Callback(string code, string error)
        {
            if (error != null)
            {
                return RedirectToAction("Login", "Auth", new {error = "Login cancelled by the user"});
            }


            var response = await new HttpClient().PostAsJsonAsync("https://api.dawreck.dev/oauth/token",
                new
                {
                    grant_type = "authorization_code",
                    client_id = "36113711",
                    code
                });
            
            var apiAccessTokenResponse = JsonConvert.DeserializeObject<LightningAccessTokenResponse>(response.Content.ReadAsStringAsync().Result);

            var accessToken = apiAccessTokenResponse.access_token;
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(accessToken);

            var userId = token.Subject;

            var userExists = _userLogic.GetUserByLightningId(userId);
            if (userExists == null)
            {

                var httpClient = new HttpClient();
                
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", accessToken);

                response =  await httpClient.GetAsync("https://api.dawreck.dev/oauth/me");

                var apiUserResponse =
                    JsonConvert.DeserializeObject<LightningUserResponse>(response.Content.ReadAsStringAsync().Result);

                var email = apiUserResponse.email;
                var firstname = apiUserResponse.firstname;
                var lastname = apiUserResponse.lastname;
                
                await _userLogic.RegisterUser("", "", firstname, lastname, userId);
                await _userLogic.LoginUser(HttpContext, "", firstname, lastname);

                return RedirectToAction("Index", "Home");    
            }

            await _userLogic.LoginUser(HttpContext, userExists.Email, userExists.Firstname, userExists.Lastname);
            

            return RedirectToAction("Index", "Home");
        }
        
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Auth", new {succes = "Logged out successfully!"});
        }
    }
}