﻿using System.Linq;
using GameShop.BAL;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameShop.Controllers
{
    public class GamesController : Controller
    {
        private readonly IUserLogic _userLogic;
        private readonly IGameLogic _gameLogic;
        private readonly ILogic<Publisher> _publisherLogic;
        private readonly ILogic<Review> _reviewLogic;
        private readonly ILogic<Genre> _genreLogic;
        private readonly IPurchaseLogic _purchaseLogic;    

        public GamesController(IUserLogic userLogic, IGameLogic gameLogic, ILogic<Publisher> publisherLogic,
            ILogic<Genre> genreLogic, ILogic<Review> reviewLogic, IPurchaseLogic purchaseLogic)
        {
            _userLogic = userLogic;
            _gameLogic = gameLogic;
            _publisherLogic = publisherLogic;
            _reviewLogic = reviewLogic;
            _genreLogic = genreLogic;
            _purchaseLogic = purchaseLogic;
        }

        public IActionResult All()
        {
            return View("All",
                new GamesViewModel
                {
                    Games = _gameLogic.GetAll(), Publishers = _publisherLogic.GetAll(),
                    Genres = _genreLogic.GetAll()
                });
        }

        [HttpPost]
        public IActionResult All(GamesViewModel gamesViewModel)
        {
            gamesViewModel.Publishers = _publisherLogic.GetAll();
            gamesViewModel.Genres = _genreLogic.GetAll();
            gamesViewModel.Games = _gameLogic.SearchForGame(gamesViewModel.GenreId, gamesViewModel.Keyword,
                gamesViewModel.PublisherId);
            
            return View("All", gamesViewModel);
        }

        public IActionResult Get(int id)
        {   
            var game = _gameLogic.GetGameByIdWithReviews(id);


            if (game == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View("Get", new GameViewModel {Game = game});
        }

        [HttpPost]
        [Authorize]
        public IActionResult BuyWithMoney(int id)
        {
            var game = _gameLogic.GetById(id);

            if (game == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);

            _purchaseLogic.PurchaseWithMoney(user, game);

            return RedirectToAction("Purchases", "Account", new {succes = "Game has been added to your account!"});
        }

        [HttpPost]
        [Authorize]
        public IActionResult BuyWithBucks(int id)
        {
            var game = _gameLogic.GetById(id);

            if (game == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);

            return !_purchaseLogic.PurchaseWithBucks(user, game) ? RedirectToAction("Get", "Games", new {id, error = "You do not have enough bucks"}) : RedirectToAction("Purchases", "Account", new {succes = "Game has been added to your account!"});
        }

        [HttpGet]
        [Authorize]
        public IActionResult Review(int id)
        {
            var game = _gameLogic.GetById(id);

            if (game == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new GameReviewViewModel());
        }

        [HttpPost]
        [Authorize]
        public IActionResult Review(int id, GameReviewViewModel dto)
        {
            var game = _gameLogic.GetById(id);

            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);
            var review = new Review {Content = dto.content, Game_Id = game.Id, Author_Id = 4};

            _reviewLogic.Save(review);

            return RedirectToRoute("GetGame", new {id});
        }
    }
}