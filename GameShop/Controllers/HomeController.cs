﻿using System.Linq;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Repositories;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace GameShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPurchaseLogic _purchaseLogic;
        
        public HomeController(IPurchaseLogic purchaseLogic)
        {
            _purchaseLogic = purchaseLogic;
        }
        
        public IActionResult Index()
        {
            return View("Index");
        }

        public IActionResult Stats()
        {
            return View(new StatsViewModel {UserPurchaseStats = _purchaseLogic.GetUserPurchaseStats().ToList(), GamePurchaseStats = _purchaseLogic.GetGamePurchaseStats().ToList()});
        }
        
        public IActionResult Error404()
        {
            return View();
        }
    }
}