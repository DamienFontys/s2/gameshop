using System.Linq;
using DevOne.Security.Cryptography.BCrypt;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameShop.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserLogic _userLogic;
        private readonly IPurchaseLogic _purchaseLogic;

        public AccountController(IUserLogic userLogic,
            IPurchaseLogic purchaseLogic)
        {
            _purchaseLogic = purchaseLogic;
            _userLogic = userLogic;            
        }

        [Authorize]
        [HttpGet]
        public IActionResult Account()
        {            
            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);

            ViewBag.User = user;
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult Account(AccountEditViewModel model)
        {
            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);
            
            if (ModelState.IsValid)
            {
                if (model.Password != model.RepeatPassword)
                {
                    return View();
                }

                user.Password = BCryptHelper.HashPassword(model.Password, BCryptHelper.GenerateSalt());
                _userLogic.Update(user);
            }

            ViewBag.User = user;
            return View(model);
        }

        [Authorize]
        public IActionResult Purchases()
        {   
            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "email")?.Value;
            var user = _userLogic.GetUserByEmail(email);
            
            return View(new AccountPurchasesViewModel {Purchases = _purchaseLogic.GetPurchasesByUser(user)});
        }
    }
}