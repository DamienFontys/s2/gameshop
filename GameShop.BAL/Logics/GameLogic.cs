using System.Collections.Generic;
using GameShop.BAL.Interfaces;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.BAL.Logics
{
    public class GameLogic: Logic<Game>, IGameLogic
    {
        private readonly IGameRepository _gameRepository;

        public GameLogic(IGameRepository gameRepository) : base(gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public IEnumerable<Game> SearchForGame(int genreId, string keyword, int publisherId)
        {
            return _gameRepository.SearchForGame(genreId, keyword, publisherId);
        }

        public Game GetGameByIdWithReviews(int id)
        {
            return _gameRepository.GetGameByIdWithReviews(id);
        }
    }
}