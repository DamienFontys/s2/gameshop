using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;
using GameShop.BAL.Interfaces;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace GameShop.BAL.Logics
{
    public class UserLogic: Logic<User>, IUserLogic
    {
        private readonly IUserRepository _userRepository;

        public UserLogic(IUserRepository userRepository) : base(userRepository)
        {
            _userRepository = userRepository;
        }

        public User GetUserByEmail(string email)
        {
            return _userRepository.GetUserByEmail(email);
        }

        public User GetUserByLightningId(string lightningId)
        {
            return _userRepository.GetUserByLightningId(lightningId);
        }
        
        public Task<bool> ValidateCredentials(string email, string password, out User user)
        {
            user = null;

            if (email == "")
            {
                return Task.FromResult(false);
            }
            
            var databaseUser = _userRepository.GetUserByEmail(email);

            if (databaseUser == null)
            {
                return Task.FromResult(false);
            }

            if (password == "")
            {
                return Task.FromResult(false);
            }
            
            if (!BCryptHelper.CheckPassword(password,databaseUser.Password))
            {
                return Task.FromResult(false);
            }

            user = databaseUser;
            return Task.FromResult(true);
        }

        public async Task LoginUser(HttpContext context, string email, string firstname, string lastname)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, email),
                new Claim("email", email),
                new Claim("firstname", firstname),
                new Claim("lastname", lastname),
            };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, "email", null);
            var principal = new ClaimsPrincipal(identity);
            
            await context.SignInAsync(principal);
        }

        public Task RegisterUser(string email, string password, string firstname, string lastname, string lightningId)
        {
            var user = new User
            {
                Email = email,
                Password = password != null ? BCryptHelper.HashPassword(password, BCryptHelper.GenerateSalt()): null,
                Firstname = firstname,
                Lastname = lastname,
                Lightningid = lightningId
            };
            
            _userRepository.RegisterUser(user);
            return Task.CompletedTask;
        }
    }
}