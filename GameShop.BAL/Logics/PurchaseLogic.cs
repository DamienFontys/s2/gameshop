using System;
using System.Collections.Generic;
using GameShop.BAL.Interfaces;
using GameShop.DAL;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;

namespace GameShop.BAL.Logics
{
    public class PurchaseLogic: Logic<Purchase>, IPurchaseLogic
    {
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IUserRepository _userRepository;

        public PurchaseLogic(IPurchaseRepository purchaseRepository, IUserRepository userRepository) : base(purchaseRepository)
        {
            _purchaseRepository = purchaseRepository;
            _userRepository = userRepository;
        }

        public IEnumerable<Purchase> GetPurchasesByUser(User user)
        {
            return _purchaseRepository.GetPurchasesForUser(user);
        }

        public bool PurchaseWithBucks(User user, Game game)
        {
            if (user.GBucks < game.CoinPrice)
            {
                return false;
            }
            
            user.GBucks -= game.CoinPrice;
            user.GBucks += 100;
            _userRepository.Update(user);

            var purchase = new Purchase {User_Id = user.Id, game_id = game.Id, gamekey = Guid.NewGuid().ToString()};
            _purchaseRepository.Add(purchase);
            return true;
        }

        public bool PurchaseWithMoney(User user, Game game)
        {
            user.GBucks += 100;
            _userRepository.Update(user);
            
            var purchase = new Purchase {User_Id = user.Id, game_id = game.Id, gamekey = Guid.NewGuid().ToString()};
            _purchaseRepository.Add(purchase);

            return true;
        }

        public IEnumerable<UserPurchaseStat> GetUserPurchaseStats()
        {
            return _purchaseRepository.GetUserPurchaseStats();
        }

        public IEnumerable<GamePurchaseStat> GetGamePurchaseStats()
        {
            return _purchaseRepository.GetGamePurchaseStats();
        }
    }
}