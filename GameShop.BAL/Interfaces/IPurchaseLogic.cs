using System.Collections;
using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.BAL.Interfaces
{
    public interface IPurchaseLogic: ILogic<Purchase>
    {
        IEnumerable<Purchase> GetPurchasesByUser(User user);
        bool PurchaseWithBucks(User user, Game game);
        bool PurchaseWithMoney(User user, Game game);

        IEnumerable<UserPurchaseStat> GetUserPurchaseStats();
        IEnumerable<GamePurchaseStat> GetGamePurchaseStats();
    }
}