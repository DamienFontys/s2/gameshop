using System.Collections;
using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.BAL.Interfaces
{
    public interface IGameLogic: ILogic<Game>
    {
        IEnumerable<Game> SearchForGame(int genreId, string keyword, int publisherId);

        Game GetGameByIdWithReviews(int id);
    }
}