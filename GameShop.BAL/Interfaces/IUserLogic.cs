using System.Threading.Tasks;
using GameShop.DAL.Models;
using Microsoft.AspNetCore.Http;

namespace GameShop.BAL.Interfaces
{
    public interface IUserLogic: ILogic<User>
    {
        User GetUserByEmail(string email);
        User GetUserByLightningId(string lightningId);
        Task<bool> ValidateCredentials(string email, string password, out User user);
        Task LoginUser(HttpContext httpContext, string email, string firstname, string lastname);

        Task RegisterUser(string email, string password, string firstname, string lastname, string lightningId);
    }
}