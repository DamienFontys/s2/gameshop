using System.Collections.Generic;
using GameShop.DAL;
using GameShop.DAL.Models;

namespace GameShop.BAL
{
    public class Logic<T>: ILogic<T> where T: Model
    {
        private readonly IRepository<T> _repository;

        public Logic(IRepository<T> repository)
        {
            _repository = repository;
        }

        public T GetById(int id)
        {
            return _repository.GetById(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public int Save(T model)
        {
            return _repository.Add(model);
        }

        public void Remove(T model)
        {
            _repository.Remove(model);
        }

        public void Update(T model)
        {
            _repository.Update(model);
        }
    }
}