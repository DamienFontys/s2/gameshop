using System.Collections;
using System.Collections.Generic;
using GameShop.DAL.Models;

namespace GameShop.BAL
{
    public interface ILogic<T> where T: Model
    {
        T GetById(int id);
        IEnumerable<T> GetAll();

        int Save(T model);
        void Remove(T model);
        void Update(T model);
    }
}