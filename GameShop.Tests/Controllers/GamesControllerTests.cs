using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Autofac.Extras.Moq;
using GameShop.BAL;
using GameShop.BAL.Interfaces;
using GameShop.Controllers;
using GameShop.DAL;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace GameShop.Tests.Controllers
{
    public class GamesControllerTests
    {
        private readonly List<Game> _testGames;
        private readonly List<Publisher> _testPublishers;
        private readonly List<Genre> _testGenres;
        private readonly List<Review> _testReviews;

        private readonly GamesController _fakeGamesController;
        
        public GamesControllerTests()
        {
            _testGames = new List<Game>();
            _testPublishers = new List<Publisher>();
            _testGenres = new List<Genre>();
            _testReviews = new List<Review>();
            
            var mock = AutoMock.GetLoose();


            mock.Mock<IGameLogic>().Setup(x => x.GetAll()).Returns(() => _testGames);
            mock.Mock<IGameLogic>().Setup(x => x.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                return _testGames.FirstOrDefault(g => g.Id == id);
            });
            mock.Mock<IGameLogic>().Setup(x => x.GetGameByIdWithReviews(It.IsAny<int>())).Returns((int id) =>
            {
                var game = _testGames.FirstOrDefault(g => g.Id == id);

                if (game == null)
                {
                    return null;
                }
                
                game.Reviews = _testReviews.Where(r => r.Game_Id == id).ToList();

                return game;
            });

            mock.Mock<IGameLogic>().Setup(x => x.SearchForGame(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns((int genreId, string keyword, int publisherId) =>
                {
                    var games = _testGames;
                    if (genreId != 0 && publisherId == 0)
                    {
                        games = games.Where(g => g.Genre_Id == genreId).ToList();
                    }

                    if (genreId == 0 && publisherId != 0)
                    {
                        games = games.Where(g => g.Publisher_Id == publisherId).ToList();
                    }

                    if (genreId != 0 && publisherId != 0)
                    {
                        games = games.Where(g => g.Genre_Id == genreId && g.Publisher_Id == publisherId).ToList();
                    }

                    if (keyword != null)
                    {
                        games = games.Where(game => game.Title.ToLower().Contains(keyword.ToLower())).ToList();
                    }
                    
                    return games;
                });
            
            
            mock.Mock<ILogic<Publisher>>().Setup(x => x.GetAll()).Returns(() => _testPublishers);
            mock.Mock<ILogic<Genre>>().Setup(x => x.GetAll()).Returns(() => _testGenres);
            
            _fakeGamesController = mock.Create<GamesController>();
        }
        
        [Fact]
        public void AllMethod_WithNoGames_ReturnsViewWithEmptyListOfGames()
        {
            //Arrange
           _testGames.Clear();
            
            //Act
            var result = _fakeGamesController.All();
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.Empty(model.Games);           
        }

        [Fact]
        public void AllMethod_WithOneGame_ReturnsViewWithListWithOneGame()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1});
            
            //Act
            var result = _fakeGamesController.All();

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.Single(model.Games);
        }

        [Fact]
        public void AllMethod_WithMultipleGames_ReturnsViewWithListWithMultipleGames()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game());
            _testGames.Add(new Game());
            
            //Act
            var result = _fakeGamesController.All();

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.Equal(2, model.Games.Count());
        }

        [Fact]
        public void GetMethod_OnANonExistingGame_ReturnsRedirectToAllPage()
        {
            //Arrange
            _testGames.Clear();
            
            //Act
            var result = _fakeGamesController.Get(1);

            //Assert
            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void GetMethod_OnAnExistingGame_ReturnsIndividualGamePage()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame"});
            
            //Act
            var result = _fakeGamesController.Get(1);

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GameViewModel;
            Assert.NotNull(model.Game);
            Assert.Equal("TestGame", model.Game.Title);
        }

        [Fact]
        public void GetMethod_OnAnExistingGameWithNoReviews_ReturnsAnIndividualGamePageWithAGameWithAnEmptyReviewsList()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1});
            
            //Act
            var result = _fakeGamesController.Get(1);

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GameViewModel;
            Assert.NotNull(model.Game);
            Assert.Empty(model.Game.Reviews);
        }

        [Fact]
        public void
            GetMethod_OnAnExistingGameWithMultipleReviews_ReturnsAnIndividualGamePageWithAGameWithMultipleReviews()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1});
            
            _testReviews.Add(new Review{Game_Id = 1});
            _testReviews.Add(new Review{Game_Id = 1});
            
            //Act
            var result = _fakeGamesController.Get(1);

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GameViewModel;
            Assert.NotNull(model.Game);
            Assert.Equal(2, model.Game.Reviews.Count);
        }

        [Fact]
        public void PostAllMethod_WithNoSearchParameters_ReturnsTheAllPageWithAllTheGames()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game());
            
            //Act
            var result = _fakeGamesController.All(new GamesViewModel());
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.Equal(_testGames, model.Games.ToList());
        }

        [Fact]
        public void PostAllMethod_WithGenreSearchParameter_ReturnsTheAllPageWithGamesForThatGenre()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Genre_Id = 1});
            _testGames.Add(new Game{Genre_Id = 2});
            
            //Act
            var result = _fakeGamesController.All(new GamesViewModel {GenreId = 1});

            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.NotEqual(_testGames, model.Games.ToList());
            Assert.Single(model.Games);
            Assert.Equal(2, _testGames.Count);
        }

        [Fact]
        public void PostAllMethod_WithPublisherParameter_ReturnsTheAllPageWithGamesForThatPublisher()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Publisher_Id = 1});
            _testGames.Add(new Game{Publisher_Id = 2});
            
            //Act
            var result = _fakeGamesController.All(new GamesViewModel {PublisherId = 1});
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.NotEqual(_testGames, model.Games.ToList());
            Assert.Single(model.Games);
            Assert.Equal(2, _testGames.Count);
        }

        [Fact]
        public void PostAllMethod_WithKeywordParameter_ReturnsTheAllPageWithGamesForThatKeyword()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Title = "testing"});
            _testGames.Add(new Game{Title = "tester"});
            
            //Act
            var result = _fakeGamesController.All(new GamesViewModel {Keyword =  "ing"});
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as GamesViewModel;
            Assert.NotEqual(_testGames, model.Games.ToList());
            Assert.Single(model.Games);
            Assert.Equal(2, _testGames.Count);
            
        }
//        [Fact]
        public void PostReviewMethod_OnExistingGame_ReturnsARedirectToGamePage()
        {
            //Arrange
            

            var user = new User { Id = 1, Email = "test@test.com", Firstname = "test", Lastname = "test" };
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim("email", user.Email),
                new Claim("firstname", user.Firstname),
                new Claim("lastname", user.Lastname)
            };
            
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, "email", null);
            var principal = new ClaimsPrincipal(identity);       

            _fakeGamesController.ControllerContext.HttpContext.SignInAsync(principal);
            
            //Act
//            var result = controller.Review(1, new GameReviewViewModel {content = "Dit is een testreview"});

            //Assert
//            Assert.IsType<RedirectToRouteResult>(result);
        }
    }
}