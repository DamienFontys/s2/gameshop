using System.Collections.Generic;
using Autofac.Extras.Moq;
using GameShop.BAL.Interfaces;
using GameShop.Controllers;
using GameShop.DAL.Models;
using GameShop.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace GameShop.Tests.Controllers
{
    public class HomeControllerTests
    {
        private readonly List<UserPurchaseStat> _testUserPurchases;
        private readonly List<GamePurchaseStat> _testGamePurchases;
        private readonly HomeController _fakeHomeController;
        
        public HomeControllerTests()
        {
            _testUserPurchases = new List<UserPurchaseStat>();
            _testGamePurchases = new List<GamePurchaseStat>();
            
            var mock = AutoMock.GetLoose();

            mock.Mock<IPurchaseLogic>().Setup(x => x.GetUserPurchaseStats()).Returns(() => _testUserPurchases);
            mock.Mock<IPurchaseLogic>().Setup(x => x.GetGamePurchaseStats()).Returns(() => _testGamePurchases);
            
            _fakeHomeController = mock.Create<HomeController>();
        }
        
        [Fact]
        public void IndexMethod_ReturnsTheIndexView()
        {
            //Arrange
            
            //Act
            var result = _fakeHomeController.Index();

            //Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void Error404Method_ReturnsThe404View()
        {
            //Arrange
            
            //Act
            var result = _fakeHomeController.Error404();
            
            //Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void StatsMethod_WithNoUserPurchaseStats_ReturnsTheStatsViewWithNoUserPurchaseStats()
        {
            //Arrange
            _testUserPurchases.Clear();
            
            //Act
            var result = _fakeHomeController.Stats();
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as StatsViewModel;
            Assert.Empty(model.UserPurchaseStats); 
        }

        [Fact]
        public void StatsMethod_WithMultipleUserPurchaseStats_ReturnsTheStatsViewWithMultipleUserPurchaseStats()
        {
            //Arrange
            _testUserPurchases.Clear();
            _testUserPurchases.Add(new UserPurchaseStat());
            _testUserPurchases.Add(new UserPurchaseStat());
            _testUserPurchases.Add(new UserPurchaseStat());
            //Act
            var result = _fakeHomeController.Stats();
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as StatsViewModel;
            Assert.NotEmpty(model.UserPurchaseStats);
            Assert.Equal(3, model.UserPurchaseStats.Count);
        }

        [Fact]
        public void StatsMethod_WithNoGamePurchaseStats_ReturnsTheStatsViewPageWithNoGamePurchaseStats()
        {
            //Arrange
            _testGamePurchases.Clear();
            
            //Act
            var result = _fakeHomeController.Stats();
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as StatsViewModel;
            Assert.Empty(model.GamePurchaseStats); 
        }
        
        [Fact]
        public void StatsMethod_WithMultipleGamePurchaseStats_ReturnsTheStatsViewWithMultipleGamePurchaseStats()
        {
            //Arrange
            _testGamePurchases.Clear();
            _testGamePurchases.Add(new GamePurchaseStat());
            _testGamePurchases.Add(new GamePurchaseStat());
            _testGamePurchases.Add(new GamePurchaseStat());
            //Act
            var result = _fakeHomeController.Stats();
            
            //Assert
            var model = Assert.IsType<ViewResult>(result).Model as StatsViewModel;
            Assert.NotEmpty(model.GamePurchaseStats);
            Assert.Equal(3, model.GamePurchaseStats.Count);
        }
    }
}