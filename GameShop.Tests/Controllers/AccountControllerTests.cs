using System.Collections.Generic;
using System.Security.Claims;
using Autofac.Extras.Moq;
using GameShop.Controllers;
using GameShop.DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace GameShop.Tests.Controllers
{
    public class AccountControllerTests
    {
        private AccountController _fakeAccountController;

        public AccountControllerTests()
        {
            var mock = AutoMock.GetLoose();

            _fakeAccountController = mock.Create<AccountController>();
        }

//        [Fact]
        public async void AccountMethod_WhenLoggedIn_ReturnsTheAccountPage()
        {
            //Arrange
            var user = new User { Id = 1, Email = "test@test.com", Firstname = "test", Lastname = "test" };
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim("email", user.Email),
                new Claim("firstname", user.Firstname),
                new Claim("lastname", user.Lastname),
            };
            
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, "email", null);
            var principal = new ClaimsPrincipal(identity);

            await _fakeAccountController.ControllerContext.HttpContext.SignInAsync(principal);
            
            //Act
            var result = _fakeAccountController.Account();
            
            //Assert
            Assert.IsType<ViewResult>(result);

        }

//        [Fact]
        public void AccountMethod_WhenNotLoggedIn_ReturnsTheLoginPage()
        {
            //Arrange
            
            
            //Act
            var result = _fakeAccountController.Account();

            //Assert
            Assert.IsType<ViewResult>(result);

        }
    }
}