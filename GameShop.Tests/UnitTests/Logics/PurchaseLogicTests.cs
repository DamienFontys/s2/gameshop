using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Logics
{
    public class PurchaseLogicTests
    {
        private readonly List<Purchase> _testModels;
        private readonly IPurchaseLogic _fakePurchaseLogic;
        
        public PurchaseLogicTests()
        {
            _testModels = new List<Purchase>();
            
            var mock = AutoMock.GetLoose();

            mock.Mock<IPurchaseRepository>().Setup(x => x.GetPurchasesForUser(It.IsAny<User>())).Returns((User user) =>
                {
                    return _testModels.Where(purchase => purchase.User_Id == user.Id);
                });
            mock.Mock<IPurchaseRepository>().Setup(x => x.Add(It.IsAny<Purchase>())).Returns((Purchase purchase) =>
            {
                _testModels.Add(purchase);
                return _testModels.IndexOf(purchase) + 1;
            });
            
            _fakePurchaseLogic = mock.Create<PurchaseLogic>();
        }

        [Fact]
        public void GettingPurchasesForAUser_ThatHasNoPurchases_Returns_AnEmptyList()
        {
            //Arrange
            _testModels.Clear();
            
            var user = new User{Id = 1};
            
            //Act
            var purchases = _fakePurchaseLogic.GetPurchasesByUser(user);

            //Assert
            Assert.Empty(purchases);
        }

        [Fact]
        public void GettingPurchasesForAUser_ThatHasPurchases_ReturnsAllPurchasesForTheUser()
        {
            //Arrange
            _testModels.Clear();
            
            var user = new User{Id = 1};
            
            _testModels.Add(new Purchase {User_Id = 1, game_id = 1, Game = new Game{Id = 1, Title = "TestGame1"}, gamekey = "thisisagamekey"});
            _testModels.Add(new Purchase {User_Id = 1, game_id = 2, Game = new Game{Id = 2, Title = "TestGame2"}, gamekey = "thisisanothergamekey"});
            _testModels.Add(new Purchase {User_Id = 2, game_id = 3, Game = new Game{Id = 3, Title = "TestGame3"}, gamekey = "thisisanothergamekey"});
            
            //Act
            var purchases = _fakePurchaseLogic.GetPurchasesByUser(user).ToList();
            
            //Assert
            Assert.NotEmpty(purchases);
            Assert.Equal(2, purchases.Count);
        }

        [Fact]
        public void PurchasingAGameWithMoney_AddsAPurchaseAndAddsBucksToTheUser()
        {
            //Arrange
            _testModels.Clear();

            var user = new User {Id = 1, GBucks = 500};
            var testGame = new Game {Id = 1};
            
            //Act
            _fakePurchaseLogic.PurchaseWithMoney(user, testGame);
            
            //Assert
            Assert.Equal(600, user.GBucks);
            Assert.NotEmpty(_testModels);
            Assert.Equal(1, (int)_testModels.First().User_Id);
        }

        [Fact]
        public void PurchasingAGameWithBucks_WhenUserDoesntHaveEnoughBucks_ReturnsFalse()
        {
            //Arrange
            _testModels.Clear();

            var user = new User {Id = 1, GBucks = 400};
            var testGame = new Game {Id = 1, CoinPrice = 500};
            
            //Act
            var purchased = _fakePurchaseLogic.PurchaseWithBucks(user, testGame);
            
            //Assert
            Assert.False(purchased);
        }

        [Fact]
        public void PurchasingAGameWithBucks_WhenUserHasEnoughBucks_ReturnsFalse()
        {
            //Arrange
            _testModels.Clear();

            var user = new User {Id = 1, GBucks = 500};
            var testGame = new Game {Id = 1, CoinPrice = 500};
            
            //Act
            var purchased = _fakePurchaseLogic.PurchaseWithBucks(user, testGame);
            
            //Assert
            Assert.True(purchased);
            Assert.Equal(100, user.GBucks);
            Assert.NotEmpty(_testModels);
            Assert.Equal(1, (int)_testModels.First().User_Id);
        }
    }
}