using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Logics
{
    public class UserLogicTests
    {
        private readonly List<User> _testModels;
        private readonly IUserLogic _fakeUserLogic;

        public UserLogicTests()
        {
            _testModels = new List<User>();
            
            var mock = AutoMock.GetLoose();

            mock.Mock<IUserRepository>().Setup(x => x.GetUserByEmail(It.IsAny<string>())).Returns((string email) =>
            {
                return _testModels.FirstOrDefault(user => user.Email == email);
            });

            mock.Mock<IUserRepository>().Setup(x => x.RegisterUser(It.IsAny<User>())).Callback((User user) =>
                {
                    _testModels.Add(user);
                });
            
            _fakeUserLogic = mock.Create<UserLogic>();
        }

        [Fact]
        public void GettingAUserByEmail_WithANonExistingEmail_ReturnsNull()
        {
            //Arrange
            _testModels.Clear();
            
            //Act
            var user = _fakeUserLogic.GetUserByEmail("test@test.com");

            //Assert
            Assert.Null(user);
        }

        [Fact]
        public void GettingAUserByEmail_WithAnExistingEmail_ReturnsAUser()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new User{Email = "test@test.com"});
            
            //Act
            var user = _fakeUserLogic.GetUserByEmail("test@test.com");
            
            //Assert
            Assert.NotNull(user);
        }

        [Fact]
        public void RegisteringAUser_AddsTheUser()
        {
            //Arrange
            _testModels.Clear();


            //Act
            var result = _fakeUserLogic.RegisterUser("test@test.com", "test", "testing", "tester", null);
            
            //Assert
            Assert.Equal(Task.CompletedTask, result);
            Assert.NotEmpty(_testModels);
        }
    }
}