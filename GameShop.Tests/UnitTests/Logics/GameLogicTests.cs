using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.BAL.Interfaces;
using GameShop.BAL.Logics;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Logics
{
    public class GameLogicTests
    {
        private readonly List<Game> _testGames;
        private readonly List<Review> _testReviews;
        private readonly IGameLogic _fakeGameLogic;
        
        public GameLogicTests()
        {
            _testGames = new List<Game>();
            _testReviews = new List<Review>();
            
            //Setup mock repository
            var mock = AutoMock.GetLoose();
            mock.Mock<IGameRepository>().Setup(x => x.SearchForGame(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>())).Returns(
                (int genreId, string keyword, int publisherId) =>
                {
                    var games = _testGames;
                    
                    if (genreId != 0 && publisherId == 0)
                    {
                        games = games.Where(game => game.Genre_Id == genreId).ToList();
                    }

                    if (genreId == 0 && publisherId != 0)
                    {
                        games = games.Where(game => game.Publisher_Id == publisherId).ToList();
                    }

                    if (genreId != 0 && publisherId != 0)
                    {
                        games = games.Where(game => game.Publisher_Id == publisherId && game.Genre_Id == genreId).ToList();
                    }

                    games = games.Where(game => game.Title.ToLower().Contains(keyword.ToLower())).ToList();
                    
                    return games;
                });
            mock.Mock<IGameRepository>().Setup(x => x.GetGameByIdWithReviews(It.IsAny<int>())).Returns((int id) =>
            {
                var game = _testGames.FirstOrDefault(g => g.Id == id);

                if (game == null)
                {
                    return null;
                }

                game.Reviews = _testReviews.FindAll(r => r.Game_Id == id);

                return game;
            });

            _fakeGameLogic = mock.Create<GameLogic>();
        }

        [Fact]
        public void SearchingForGame_WithNoParameters_ReturnsAllGames()
        {
            //Arrange
            _testGames.Add(new Game {Title = "TestGame1", Genre_Id = 1, Publisher_Id = 1});
            _testGames.Add(new Game {Title = "TestGame2", Genre_Id = 1, Publisher_Id = 2});
            _testGames.Add(new Game {Title = "TestGame3", Genre_Id = 3, Publisher_Id = 1});
            
            //Act
            var games = _fakeGameLogic.SearchForGame(0, "", 0);


            //Assert
            Assert.Equal(_testGames, games);
        }

        [Fact]
        public void SearchingForAGame_WithGenreParameter_ReturnsAllGamesWithTheSpecifiedGenre()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game {Title = "TestGame1", Genre_Id = 1, Publisher_Id = 1});
            _testGames.Add(new Game {Title = "TestGame2", Genre_Id = 1, Publisher_Id = 2});
            _testGames.Add(new Game {Title = "TestGame3", Genre_Id = 3, Publisher_Id = 1});
            
            //Act
            var games = _fakeGameLogic.SearchForGame(1, "", 0);
            
            //Assert
            Assert.Equal(2, games.Count());
        }

        [Fact]
        public void SearchingForAGame_WithPublisherParameter_ReturnsAllGamesWithTheSpecifiedPublisher()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game {Title = "TestGame1", Genre_Id = 1, Publisher_Id = 1});
            _testGames.Add(new Game {Title = "TestGame2", Genre_Id = 1, Publisher_Id = 2});
            _testGames.Add(new Game {Title = "TestGame3", Genre_Id = 3, Publisher_Id = 1});
            
            //Act
            var games = _fakeGameLogic.SearchForGame(0, "", 1);
            
            //Assert
            Assert.Equal(2, games.Count());
        }

        [Fact]
        public void SearchingForAGame_WithKeyWordParameter_ReturnsAllGamesWithTheSpecifiedKeyword()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game {Title = "Frikandel", Genre_Id = 1, Publisher_Id = 1});
            _testGames.Add(new Game {Title = "Aardappel", Genre_Id = 1, Publisher_Id = 2});
            _testGames.Add(new Game {Title = "Frikandellenbroodje", Genre_Id = 3, Publisher_Id = 1});
            
            //Act
            var games = _fakeGameLogic.SearchForGame(0, "frikandel", 0);

            //Assert
            Assert.Equal(2, games.Count());
        }

        [Fact]
        public void GettingAGameByIdWithReviews_WithNoReviews_ReturnsAGameWithAnEmptyReviewsList()
        {
            //Arrange
            _testGames.Clear();
            
            _testGames.Add(new Game{Id = 1});
            
            //Act
            var game = _fakeGameLogic.GetGameByIdWithReviews(1);

            //Assert
            Assert.NotNull(game);
            Assert.Empty(game.Reviews);
        }

        [Fact]
        public void GettingAGameByIdWithReviews_WithReviews_ReturnsAGameWithAFilledReviewList()
        {
            //Arrange
            _testGames.Clear();
            
            _testGames.Add(new Game{Id = 1});
            _testReviews.Add(new Review{Game_Id = 1});
            
            //Act
            var game = _fakeGameLogic.GetGameByIdWithReviews(1);
            
            //Assert
            Assert.NotNull(game);
            Assert.NotEmpty(game.Reviews);
        }
    }
}