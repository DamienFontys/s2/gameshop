using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Repositories
{
    public class PurchaseRepositoryTests
    {
        private readonly List<Purchase> _testPurchases;
        private readonly List<UserPurchaseStat> _userPurchaseStats;
        private readonly List<GamePurchaseStat> _gamePurchaseStats;
        private readonly PurchaseRepository _fakePurchaseRepository;

        public PurchaseRepositoryTests()
        {
            _testPurchases = new List<Purchase>();
            _userPurchaseStats = new List<UserPurchaseStat>();
            _gamePurchaseStats = new List<GamePurchaseStat>();
            var mock = AutoMock.GetLoose();

            mock.Mock<IPurchaseContext>().Setup(x => x.GetPurchasesForUser(It.IsAny<User>())).Returns((User user) =>
                {
                    return _testPurchases.Where(p => p.User_Id == user.Id);
                });

            mock.Mock<IPurchaseContext>().Setup(x => x.GetUserPurchaseStats()).Returns(() => _userPurchaseStats);
            mock.Mock<IPurchaseContext>().Setup(x => x.GetGamePurchaseStats()).Returns(() => _gamePurchaseStats);
            
            _fakePurchaseRepository = mock.Create<PurchaseRepository>();
        }

        [Fact]
        public void GettingPurchasesForUser_WithNoPurchases_ReturnAnemptyList()
        {
            //Arrange
            _testPurchases.Clear();
            var user = new User {Id = 1};
            //Act
            var purchases = _fakePurchaseRepository.GetPurchasesForUser(user);
            
            //Assert
            Assert.Empty(purchases);
        }

        [Fact]
        public void GettingPurchasesForUser_WithMultiplePurchases_ReturnAListWithMultiplePurchases()
        {
            //Arrange
            _testPurchases.Clear();
            var user = new User {Id = 1};
            _testPurchases.Add(new Purchase{User_Id = 1});
            _testPurchases.Add(new Purchase{User_Id = 1});
            
            //Act
            var purchases = _fakePurchaseRepository.GetPurchasesForUser(user);
            
            //Assert
            Assert.NotEmpty(purchases);
            Assert.Equal(2, purchases.Count());
        }

        [Fact]
        public void GettingUserPurchaseStats_WithoutAnyPurchases_ReturnsAListWithAllUsersAndPurchases()
        {
           //Arrange
            _userPurchaseStats.Clear();
            
            //Act
            var purchaseStats = _fakePurchaseRepository.GetUserPurchaseStats();
            
            //Assert
            Assert.Empty(purchaseStats);
        }

        [Fact]
        public void GettingGamePurchaseStats_WithoutAnyPurchases_ReturnsAListWithAllGamePurchases()
        {
            //Arrange
            _gamePurchaseStats.Clear();
            
            //Act
            var purchaseStats = _fakePurchaseRepository.GetGamePurchaseStats();
            
            //Assert
            Assert.Empty(purchaseStats);
        }
    }
}