using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Repositories
{
    public class GameRepositoryTests
    {
        private readonly List<Game> _testGames;
        private readonly List<Review> _testReviews;
        private readonly GameRepository _fakeGameRepository;
        
        public GameRepositoryTests()
        {
            _testGames = new List<Game>();
            _testReviews = new List<Review>();

            var mock = AutoMock.GetLoose();
            mock.Mock<IGameContext>().Setup(x => x.GetAll()).Returns(() => _testGames);
            mock.Mock<IGameContext>().Setup(x => x.GetById(It.IsAny<int>()))
                .Returns((int id) => _testGames.FirstOrDefault(m => m.Id == id));
            mock.Mock<IGameContext>().Setup(x => x.GetGameByIdWithReviews(It.IsAny<int>())).Returns((int id) =>
            {
                var game = _testGames.FirstOrDefault(g => g.Id == id);

                if (game == null)
                {
                    return null;
                }

                game.Reviews = _testReviews.FindAll(r => r.Game_Id == id);

                return game;
            });
            _fakeGameRepository = mock.Create<GameRepository>();
        }
        
        [Fact]
        public void SearchingForAGame_WithoutAnySearchParameter_ReturnsAListWithAllGames()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame1", Description = "TestGame1", Price = 99.99, CoinPrice = 500});
            _testGames.Add(new Game{Id = 2, Title = "TestGame2", Description = "TestGame2", Price = 99.99, CoinPrice = 500});
            
            //Act
            var searchResult = _fakeGameRepository.SearchForGame(0, null, 0);

            //Assert
            Assert.Equal(_fakeGameRepository.GetAll(), searchResult);
        }

        [Fact]
        public void SearchingForAGame_WithKeyWordParameter_ReturnsAListOfGamesWithTitleContainingKeyword()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame1BlackOps", Description = "TestGame1", Price = 99.99, CoinPrice = 500});
            _testGames.Add(new Game{Id = 2, Title = "TestGame2Minecraft", Description = "TestGame2", Price = 99.99, CoinPrice = 500});
            //Act
            var searchResult = _fakeGameRepository.SearchForGame(0, "Mine", 0).ToList();


            //Assert
            Assert.Single(searchResult);
            Assert.Equal(2, (int)searchResult.First().Id);
        }

        [Fact]
        public void SearchingForAGame_WithGenreParameter_ReturnsAListOfGamesWithCorrectGenre()
        {
            //Arrange
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame1", Description = "TestGame1", Price = 99.99, CoinPrice = 500, Genre_Id = 1});
            _testGames.Add(new Game{Id = 2, Title = "TestGame2", Description = "TestGame2", Price = 99.99, CoinPrice = 500, Genre_Id = 2});
            
            //Act
            var searchResult = _fakeGameRepository.SearchForGame(1, null, 0);

            //Assert
            Assert.Single(searchResult);
            Assert.Equal(1, (int)searchResult.First().Id);
            Assert.Equal(1, (int)searchResult.First().Genre_Id);
        }

        [Fact]
        public void SearchingForAGame_WithPublisherParameter_ReturnsAListOfGamesWithCorrectPublisher()
        {
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame1", Description = "TestGame1", Price = 99.99, CoinPrice = 500, Publisher_Id = 1});
            _testGames.Add(new Game{Id = 2, Title = "TestGame2", Description = "TestGame2", Price = 99.99, CoinPrice = 500, Publisher_Id = 2});
            
            //Act
            var searchResult = _fakeGameRepository.SearchForGame(0, null, 1);

            //Assert
            Assert.Single(searchResult);
            Assert.Equal(1, (int)searchResult.First().Id);
            Assert.Equal(1, (int)searchResult.First().Publisher_Id);
        }

        [Fact]
        public void SearchingForAGame_WithEveryParameter_ReturnsAListOfGamesWithCorrectParameterCorrelations()
        {
            _testGames.Clear();
            _testGames.Add(new Game{Id = 1, Title = "TestGame1Black", Description = "TestGame1", Price = 99.99, CoinPrice = 500, Genre_Id = 2, Publisher_Id = 1});
            _testGames.Add(new Game{Id = 2, Title = "TestGame2Black", Description = "TestGame2", Price = 99.99, CoinPrice = 500, Genre_Id = 3, Publisher_Id = 1});
            _testGames.Add(new Game{Id = 3, Title = "TestGame3White", Description = "TestGame3", Price = 99.99, CoinPrice = 500, Genre_Id = 3, Publisher_Id = 1});
            _testGames.Add(new Game{Id = 4, Title = "TestGame4White", Description = "TestGame4", Price = 99.99, CoinPrice = 500, Genre_Id = 2, Publisher_Id = 2});
            
            //Act
            var searchResult = _fakeGameRepository.SearchForGame(2, "game", 2);

            //Assert
            Assert.Single(searchResult);
            Assert.Equal(4, (int)searchResult.First().Id);
        }

        [Fact]
        public void GettingAGameByIdWithReviews_WithNoReviews_ReturnsAGameWithAnEmptyReviewsList()
        {
            //Arrange
            _testGames.Clear();
            _testReviews.Clear();
            
            _testGames.Add(new Game{Id = 1});
            
            //Act
            var game = _fakeGameRepository.GetGameByIdWithReviews(1);
            
            //Assert
            Assert.NotNull(game);
            Assert.Empty(game.Reviews);
        }

        [Fact]
        public void GettingAGameByIdWithReviews_WithReviews_ReturnsAGameWithAFilledReviewsList()
        {
            //Arrange
            _testGames.Clear();
            _testReviews.Clear();
            
            _testGames.Add(new Game{Id = 1, Reviews = new List<Review>()});                        
            _testReviews.Add(new Review{Game_Id = 1});

            //Act
            var game = _fakeGameRepository.GetGameByIdWithReviews(1);

            //Assert
            Assert.NotNull(game);
            Assert.NotEmpty(game.Reviews);
        }
    }
}