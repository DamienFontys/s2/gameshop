using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.DAL.Interfaces;
using GameShop.DAL.Models;
using GameShop.DAL.Repositories;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests.Repositories
{
    public class UserRepositoryTests
    {
        private readonly List<User> _testUsers;
        private readonly UserRepository _fakeUserRepository;

        public UserRepositoryTests()
        {
            _testUsers = new List<User>();

            //Setup mock repository
            var mock = AutoMock.GetLoose();
            mock.Mock<IUserContext>().Setup(x => x.GetById(It.IsAny<int>())).Returns((int id) =>  _testUsers.FirstOrDefault(m => m.Id == id));
            mock.Mock<IUserContext>().Setup(x => x.GetAll()).Returns(() => _testUsers);
            mock.Mock<IUserContext>().Setup(x => x.Add(It.IsAny<User>())).Returns((User model) =>
            {
                model.Id = (uint)_testUsers.Count + 1;
                _testUsers.Add(model);
                return _testUsers.IndexOf(model) + 1;
            });
            mock.Mock<IUserContext>().Setup(x => x.Remove(It.IsAny<User>()))
                .Callback((User model) => _testUsers.Remove(model));
            mock.Mock<IUserContext>().Setup(x => x.Update(It.IsAny<User>()))
                .Callback((User model) => _testUsers[_testUsers.IndexOf(model)] = model);
            mock.Mock<IUserContext>().Setup(x => x.GetUserByEmail(It.IsAny<string>())).Returns((string email) =>
            {
                return _testUsers.FirstOrDefault(user => user.Email == email);
            });
            mock.Mock<IUserContext>().Setup(x => x.RegisterUser(It.IsAny<User>())).Callback((User user) =>
            {
                _testUsers.Add(user);
            });

            _fakeUserRepository = mock.Create<UserRepository>();
        }
        
        [Fact]
        public void GettingAUserByEmail_WithAnExistingEmail_ReturnsAUser()
        {
            //Arrange
            _testUsers.Clear();
            _testUsers.Add(new User{Id = 1, Email = "test@test.com"});
            //Act
            
            var user = _fakeUserRepository.GetUserByEmail("test@test.com");

            //Assert
            Assert.NotNull(user);
            Assert.IsType<User>(user);
        }

        [Fact]
        public void GettingAUserByEmail_WithANonExistingEmail_ReturnsNull()
        {
            //Arrange
            _testUsers.Clear();

            //Act
            var user = _fakeUserRepository.GetUserByEmail("test@test.com");

            //Assert
            Assert.Null(user);
        }

        [Fact]
        public void RegisteringAUser_AddsANewUserToTheContext()
        {
            //Arrange
            _testUsers.Clear();
            
            //Act
            var user = new User{Firstname = "Test", Lastname = "Tester"};
            _fakeUserRepository.RegisterUser(user);
           
            //Assert
            Assert.NotEmpty(_testUsers);
            Assert.Equal("Test", _testUsers.First().Firstname);
        }
    }
}