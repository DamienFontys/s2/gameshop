using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class ModelTests
    {
        public Model _model;

        public ModelTests()
        {
            _model = new Model();
        }

        [Fact]
        public void ModelId_CanGetAndSet()
        {
            //Arrange
            uint idProperty = 1;
            
            //Act
            _model.Id = idProperty;
            var id = _model.Id;
            
            //Assert
            Assert.Equal(idProperty, id);
        }
    }
}