using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class PublisherTests
    {
        [Fact]
        public void PublisherName_CanGetAndSet()
        {
            //Arrange
            var publisher = new Publisher();
            
            //Act
            publisher.Name = "name";
            var name = publisher.Name;

            //Assert
            Assert.Equal("name", name);
        }
    }
}