using System.Collections.Generic;
using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class UserTests
    {
        private User _user;

        public UserTests()
        {
            _user = new User();
        }

        [Fact]
        public void UserEmail_CanGetAndSet()
        {
            //Arrange
            var emailProperty = "test@test.com";
            
            //Act
            _user.Email = emailProperty;
            var email = _user.Email;
            
            //Assert
            Assert.Equal(emailProperty, email);
        }

        [Fact]
        public void UserPassword_CanGetAndSet()
        {
            //Arrange
            var passwordProperty = "password";
            
            //Act
            _user.Password = passwordProperty;
            var password = _user.Password;
            
            //Assert
            Assert.Equal(passwordProperty, password);
        }

        [Fact]
        public void UserFirstname_CanGetAndSet()
        {
            //Arrange
            var firstnameProperty = "firstname";
            
            //Act
            _user.Firstname = firstnameProperty;
            var firstname = _user.Firstname;
            
            //Assert
            Assert.Equal(firstnameProperty, firstname);
        }
        
        [Fact]
        public void UserLastname_CanGetAndSet()
        {
            //Arrange
            var lastnameProperty = "firstname";
            
            //Act
            _user.Lastname = lastnameProperty;
            var lastname = _user.Lastname;
            
            //Assert
            Assert.Equal(lastnameProperty, lastname);
        }

        [Fact]
        public void UserGbucks_CanGetAndSet()
        {
            //Arrange
            var gbucksProperty = 99;

            //Act
            _user.GBucks = gbucksProperty;
            var gbucks = _user.GBucks;
            
            //Assert
            Assert.Equal(gbucksProperty, gbucks);
        }

        [Fact]
        public void UserReviews_CanGetAndSet()
        {
            //Arrange
            var reviewsProperty = new List<Review> {new Review {Id = 1}};
            
            //Act
            _user.Reviews = reviewsProperty;
            var reviews = _user.Reviews;
            
            //Assert
            Assert.Equal(reviewsProperty, reviews);
        }

        [Fact]
        public void GettingTheGravatarHash_ReturnsTheRightGravatarHash()
        {
            //Arrange
            _user.Email = "d.pc@live.nl";
            
            //Act
            var hash = _user.GetGravatarHash();
            
            //Assert
            Assert.Equal("17d94f91b0d2b0cc54846145e1506896", hash);
        }
        
    }
}