using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class GenreTests
    {
        [Fact]
        public void GenreTitle_CanGetAndSet()
        {
            //Arrange
            var genre = new Genre();
            
            //Act
            genre.Title = "title";
            var title = genre.Title;

            //Assert
            Assert.Equal("title", title);
        }
    }
}