using System.Collections.Generic;
using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class GamePurchaseStatTests
    {
        private readonly GamePurchaseStat _gamePurchaseStat;

        public GamePurchaseStatTests()
        {
            _gamePurchaseStat = new GamePurchaseStat();
        }

        [Fact]
        public void GamePurchaseStatGame_CanGetAndSet()
        {
            //Arrange
            var gameProperty = "TestGame";
            
            //Act
            _gamePurchaseStat.Game = gameProperty;
            var game = _gamePurchaseStat.Game;

            //Assert
            Assert.Equal(gameProperty, game);
        }

        [Fact]
        public void GamePurchaseStatPurchases_CanGetAndSet()
        {
            //Arrange
            var purchasesProperty = 100;

            //Act
            _gamePurchaseStat.Purchases = purchasesProperty;
            var purchases = _gamePurchaseStat.Purchases;

            //Assert
            Assert.Equal(purchasesProperty, purchases);
        }
    }
}