using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class UserPurchaseStatTests
    {
        private readonly UserPurchaseStat _userPurchaseStat;

        public UserPurchaseStatTests()
        {
            _userPurchaseStat = new UserPurchaseStat();
        }

        [Fact]
        public void UserPurchaseStatEmail_CanGetAndSet()
        {
            //Arrange
            var emailProperty = "test@test.com";
            
            //Act
            _userPurchaseStat.Email = emailProperty;
            var email = _userPurchaseStat.Email;

            //Assert
            Assert.Equal(emailProperty, email);
        }

        [Fact]
        public void GamePurchaseStatPurchases_CanGetAndSet()
        {
            //Arrange
            var purchasesProperty = 100;

            //Act
            _userPurchaseStat.Purchases = purchasesProperty;
            var purchases = _userPurchaseStat.Purchases;

            //Assert
            Assert.Equal(purchasesProperty, purchases);
        }
    }
}