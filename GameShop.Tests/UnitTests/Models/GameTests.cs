using System.Collections.Generic;
using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class GameTests
    {
        private Game _game;

        public GameTests()
        {
            _game = new Game();
        }

        [Fact]
        public void GameTitle_CanGetAndSet()
        {
            //Arrange

            //Act
            _game.Title = "title";
            var title = _game.Title;

            //Assert
            Assert.Equal("title", title);
        }

        [Fact]
        public void GameDescription_CanGetAndSet()
        {
            //Arrange

            //Act
            _game.Description = "description";
            var description = _game.Description;

            //Assert
            Assert.Equal("description", description);
        }

        [Fact]
        public void GamePrice_CanGetAndSet()
        {
            //Arrange
            
            //Act
            _game.Price = 99.99;
            var price = _game.Price;
            
            //Assert
            Assert.Equal(99.99, price);
        }

        [Fact]
        public void GameCoinPrice_CanGetAndSet()
        {
            //Arrange
            
            //Act
            _game.CoinPrice = 99;
            var coinPrice = _game.CoinPrice;
            
            //Assert
            Assert.Equal(99, coinPrice);
        }

        [Fact]
        public void GameGenreId_CanGetAndSet()
        {
            //Arrange

            //Act
            _game.Genre_Id = 1;
            var genreId = _game.Genre_Id;
            
            //Assert
            Assert.Equal(1, (int)genreId);
        }

        [Fact]
        public void GamePublisherId_CanGetAndSet()
        {
            //Arrange
            
            //Act
            _game.Publisher_Id = 1;
            var publisherId = _game.Publisher_Id;
            
            //Assert
            Assert.Equal(1, (int)publisherId);
        }

        [Fact]
        public void GameReviews_CanGetAndSet()
        {
            //Arrange
            var reviewProperty = new List<Review> {new Review {Id = 1}};
            
            //Act
            _game.Reviews = reviewProperty;
            var reviews = _game.Reviews;
            
            //Assert
            Assert.Equal(reviewProperty, reviews);
        }
    }
}