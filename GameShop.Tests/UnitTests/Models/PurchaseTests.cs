using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class PurchaseTests
    {
        private Purchase _purchase;

        public PurchaseTests()
        {
            _purchase = new Purchase();
        }

        [Fact]
        public void PurchaseUserId_CanGetAndSet()
        {
            //Arrange
            uint userIdProperty = 1;

            //Act
            _purchase.User_Id = userIdProperty;
            var userId = _purchase.User_Id;
            
            //Assert
            Assert.Equal(userIdProperty, userId);
        }

        [Fact]
        public void PurchaseGameId_CanGetAndSet()
        {
            //Arrange
            uint gameIdProperty = 1;
            
            //Act
            _purchase.game_id = gameIdProperty;
            var gameId = _purchase.game_id;

            //Assert
            Assert.Equal(gameIdProperty, gameId);
        }

        [Fact]
        public void PurchaseGamekey_CanGetAndSet()
        {
            //Arrange
            var gamekeyProperty = "gamekey";

            //Act
            _purchase.gamekey = gamekeyProperty;
            var gamekey = _purchase.gamekey;
            
            //Assert
            Assert.Equal(gamekeyProperty, gamekey);
        }

        [Fact]
        public void PurchaseUser_CanGetAndSet()
        {
            //Arrange
            var userProperty = new User{Id = 1};
            
            //Act
            _purchase.User = userProperty;
            var user = _purchase.User;
            
            //Assert
            Assert.Equal(userProperty, user);
        }

        [Fact]
        public void PurchaseGame_CanGetAndSet()
        {
            //Arrange
            var gameProperty = new Game {Id = 1};

            //Act
            _purchase.Game = gameProperty;
            var game = _purchase.Game;

            //Assert
            Assert.Equal(gameProperty, game);
        }
    }
}