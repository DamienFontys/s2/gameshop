using GameShop.DAL.Models;
using Xunit;

namespace GameShop.Tests.UnitTests.Models
{
    public class ReviewTests
    {
        private Review _review;

        public ReviewTests()
        {
            _review = new Review();
        }

        [Fact]
        public void ReviewContent_CanGetAndSet()
        {
            //Arrange
            var contentProperty = "content";
            
            //Act
            _review.Content = contentProperty;
            var content = _review.Content;
            
            //Assert
            Assert.Equal(contentProperty, content);
        }

        [Fact]
        public void ReviewGameId_CanGetAndSet()
        {
            //Arrange
            uint gameIdProperty = 1;
            
            //Act
            _review.Game_Id = gameIdProperty;
            var gameId = _review.Game_Id;

            //Assert
            Assert.Equal(gameIdProperty, gameId);
        }

        [Fact]
        public void ReviewAuthorId_CanGetAndSet()
        {
            //Arrange
            uint authorIdProperty = 1;

            //Act
            _review.Author_Id = authorIdProperty;
            var authorId = _review.Author_Id;

            //Assert
            Assert.Equal(authorIdProperty, authorId);
        }

        [Fact]
        public void ReviewAuthor_CanGetAndSet()
        {
            //Arrange
            var authorProperty = new User{Id = 1};
            
            //Act
            _review.Author = authorProperty;
            var author = _review.Author;

            //Assert
            Assert.Equal(authorProperty, author);
        }
    }
}