using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.BAL;
using GameShop.DAL;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests
{
    public class LogicTests
    {
        private readonly List<TestModel> _testModels;
        private readonly Logic<TestModel> _fakeLogic;

        public LogicTests()
        {
            _testModels = new List<TestModel>();
            
            //Setup mock repository
            var mock = AutoMock.GetLoose();
            mock.Mock<IRepository<TestModel>>().Setup(x => x.GetById(It.IsAny<int>())).Returns((int id) =>  _testModels.FirstOrDefault(m => m.Id == id));
            mock.Mock<IRepository<TestModel>>().Setup(x => x.GetAll()).Returns(() => _testModels);
            mock.Mock<IRepository<TestModel>>().Setup(x => x.Add(It.IsAny<TestModel>())).Returns((TestModel model) =>
            {
                model.Id = (uint)_testModels.Count + 1;
                _testModels.Add(model);
                return _testModels.IndexOf(model) + 1;
            });
            mock.Mock<IRepository<TestModel>>().Setup(x => x.Remove(It.IsAny<TestModel>()))
                .Callback((TestModel model) => _testModels.Remove(model));
            mock.Mock<IRepository<TestModel>>().Setup(x => x.Update(It.IsAny<TestModel>()))
                .Callback((TestModel model) => _testModels[_testModels.IndexOf(model)] = model);

            _fakeLogic = mock.Create<Logic<TestModel>>();
        }
        

        [Fact]
        public void GettingAModelById_WithANonExistingId_ReturnsNull()
        {
            //Arrange
            _testModels.Clear();
            
            //Act
            var model = _fakeLogic.GetById(1);

            //Assert
            Assert.Null(model);
        }

        [Fact]
        public void GettingAModelById_WithAnExistingId_ReturnsAModel()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel{Id = 1});
            
            //Act
            var model = _fakeLogic.GetById(1);
            
            //Assert
            Assert.NotNull(model);
            Assert.Equal(1, (int)model.Id);
        }

        [Fact]
        public void GettingAllModels_WithNoModels_ReturnsAnEmptyList()
        {
            //Arrange
            _testModels.Clear();
            
            //Act
            var models = _fakeLogic.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Empty(models);
        }

        [Fact]
        public void GettingAllModels_WithASingleModel_ReturnsAListWithASingleItem()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel{Id = 1});

            //Act
            var models = _fakeLogic.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Single(models);
        }

        [Fact]
        public void GettingAllModels_WithMultipleModels_ReturnsAListWithMultipleItems()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel{Id = 1});
            _testModels.Add(new TestModel{Id = 2});
            _testModels.Add(new TestModel{Id = 3});
            
            //Act
            var models = _fakeLogic.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Equal(3, models.Count());
        }

        [Fact]
        public void SavingAModel_MakesItRetrievableFromTheContext()
        {
            //Arrange
            _testModels.Clear();
            
            //Act
            var testModel = new TestModel { Id = 1};
            Assert.Empty(_fakeLogic.GetAll());
            _fakeLogic.Save(testModel);
            
            //Assert
            Assert.Single(_fakeLogic.GetAll());
            Assert.Equal(testModel, _fakeLogic.GetById(1));
        }

        [Fact]
        public void RemovingAModel_MakesItUnretrievableFromTheContext()
        {
            //Arrange
            _testModels.Clear();
            var testModel = new TestModel {Id = 1};
            _testModels.Add(testModel);
           
            //Act
            Assert.Single(_fakeLogic.GetAll());
            _fakeLogic.Remove(testModel);
            var model = _fakeLogic.GetById(1);
            
            //Assert
            Assert.Null(model);
        }

        [Fact]
        public void UpdatingAModel_UpdatesTheModelInTheContext()
        {
            //Arrange
            _testModels.Clear();
            var testModel = new TestModel {Id = 1};
            _testModels.Add(testModel);

            //Act
            testModel.Id = 2;
            _fakeLogic.Update(testModel);
            var model = _fakeLogic.GetById(2);
            
            //Assert
            Assert.NotNull(model);
        }
    }
}