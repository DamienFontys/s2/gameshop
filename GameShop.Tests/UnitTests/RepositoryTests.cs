using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using GameShop.DAL;
using Moq;
using Xunit;

namespace GameShop.Tests.UnitTests
{
    public class RepositoryTests
    {
        private readonly List<TestModel> _testModels;
        private readonly Repository<TestModel> _fakeRepository; 
        public RepositoryTests()
        {
            _testModels = new List<TestModel>();
            
            //Setup mock repository
            var mock = AutoMock.GetLoose();
            mock.Mock<IContext<TestModel>>().Setup(x => x.GetById(It.IsAny<int>())).Returns((int id) =>  _testModels.FirstOrDefault(m => m.Id == id));
            mock.Mock<IContext<TestModel>>().Setup(x => x.GetAll()).Returns(() => _testModels);
            mock.Mock<IContext<TestModel>>().Setup(x => x.Add(It.IsAny<TestModel>())).Returns((TestModel model) =>
            {
                model.Id = (uint)_testModels.Count + 1;
                _testModels.Add(model);
                return _testModels.IndexOf(model) + 1;
            });
            mock.Mock<IContext<TestModel>>().Setup(x => x.Remove(It.IsAny<TestModel>()))
                .Callback((TestModel model) => _testModels.Remove(model));
            mock.Mock<IContext<TestModel>>().Setup(x => x.Update(It.IsAny<TestModel>()))
                .Callback((TestModel model) => _testModels[_testModels.IndexOf(model)] = model);

            _fakeRepository = mock.Create<Repository<TestModel>>();
        }
        
        [Fact]
        public void GettingAModelById_WithANonExistingId_ReturnsNull()
        {
            //Arrange
            _testModels.Clear();
            
            //Act
            var model = _fakeRepository.GetById(1);

            //Assert
            Assert.Null(model);
        }

        [Fact]
        public void GettingAModelById_WithAnExistingId_ReturnsAModel()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel {Id = 1});

            //Act
            var model = _fakeRepository.GetById(1);

            //Assert
            Assert.NotNull(model);
            Assert.IsType<TestModel>(model);
            Assert.Equal(1, (int) model.Id);
        }

        [Fact]
        public void GettingAllModels_WithNoModels_ReturnsAnEmptyList()
        {
            //Arrange
            _testModels.Clear();

            //Act
            var models = _fakeRepository.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Empty(models);
        }

        [Fact]
        public void GettingAllModels_WithASingleModels_ReturnsAListWithASingleModel()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel {Id = 1});
            
            //Act
            var models = _fakeRepository.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Single(models);
        }

        [Fact]
        public void GettingAllModels_WithMultipleModels_ReturnsAListWithMultipleModels()
        {
            //Arrange
            _testModels.Clear();
            _testModels.Add(new TestModel {Id = 1});
            _testModels.Add(new TestModel {Id = 2});
            _testModels.Add(new TestModel {Id = 3});
            
            //Act
            var models = _fakeRepository.GetAll();

            //Assert
            Assert.NotNull(models);
            Assert.IsType<List<TestModel>>(models);
            Assert.Equal(3, models.Count());
        }

        [Fact]
        public void AddingAModel_MakesItRetrievable()
        {
            //Arrange
            _testModels.Clear();

            //Act
            Assert.Empty(_fakeRepository.GetAll());
            _fakeRepository.Add(new TestModel {Id = 1});
            var model = _fakeRepository.GetById(1);

            //Assert
            Assert.NotNull(model);
            Assert.IsType<TestModel>(model);
            Assert.Equal(1, (int) model.Id);
        }

        [Fact]
        public void RemovingAModel_MakesItNonRetrievable()
        {
            //Arrange
            _testModels.Clear();
            var testModel = new TestModel {Id = 1};
            _testModels.Add(testModel);


            //Act
            Assert.Single(_fakeRepository.GetAll());
            _fakeRepository.Remove(testModel);
            var model = _fakeRepository.GetById(1);

            //Assert
            Assert.Null(model);
        }

        [Fact]
        public void UpdatingAModel_UpdatesTheModelInTheContext()
        {
            //Arrange
            _testModels.Clear();
            var testModel = new TestModel {Id = 1};
            _testModels.Add(testModel);

            //Act
            testModel.Id = 2;
            _fakeRepository.Update(testModel);
            var model = _fakeRepository.GetById(2);

            //Assert
            Assert.NotNull(model);
        }
    }
}