using Xunit;

namespace GameShop.Tests.UnitTests
{
    public class ContextTests
    {
        [Fact]
        public void GettingById_OnAnExistingId_ReturnsAModelInstance()
        {
            //Arrange
//            var context = new MockContext<TestModel>();
//            context.Add(new TestModel {Id = 1});

            //Act
//            var testModel = context.GetById(1);

            //Assert
//            Assert.NotNull(testModel);
//            Assert.Equal(1, (int)testModel.Id);
        }

        [Fact]
        public void GettingById_OnANonExistingId_ReturnsNull()
        {
            //Arrange
//            var context = new MockContext<TestModel>();
            
            //Act
//            var testModel = context.GetById(1);

            //Assert
//            Assert.Null(testModel);
        }

        [Fact]
        public void GettingAll_OnARepoWithOneItem_ReturnsAListWithOneItem()
        {
            //Arrange
//            var context = new MockContext<TestModel>();
//            context.Add(new TestModel {Id = 1});

            //Act
//            var testModels = context.GetAll();

            //Assert
//            Assert.NotNull(testModels);
//            Assert.Single(testModels);
        }

        [Fact]
        public void GettingAll_OnARepoWithTwoItems_ReturnsAListWithTwoItems()
        {
            //Arrange
//            var context = new MockContext<TestModel>();
//            context.Add(new TestModel {Id = 1});
//            context.Add(new TestModel {Id = 2});

            //Act
//            var testModels = context.GetAll();

            //Assert
//            Assert.NotNull(testModels);
//            Assert.Equal(2, testModels.Count());
        }

        [Fact]
        public void GettingAll_OnARepoWithMultipleItems_ReturnsAListWithMultipleItems()
        {
            //Arrange
//            var context = new MockContext<TestModel>();
//            context.Add(new TestModel {Id = 1});
//            context.Add(new TestModel {Id = 2});
//            context.Add(new TestModel {Id = 3});

            //Act
//            var testModels = context.GetAll();

            //Assert
//            Assert.NotNull(testModels);
//            Assert.Equal(3, testModels.Count());
        }
        
        [Fact]
        public void GettingAll_WithAnEmptyRepo_ReturnsAnEmptyList()
        {
            //Arrange
//            var context = new MockContext<TestModel>();

            //Act
//            var testModels = context.GetAll();

            //Assert
//            Assert.NotNull(testModels);
//            Assert.Empty(testModels);
        }
    }
}